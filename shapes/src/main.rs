#[derive(Debug)]
struct Rectangle {
    height: u32,
    width: u32,
}

impl Rectangle {
    fn square(size: u32) -> Rectangle{
        /* Associated function of Rectangle.
        Don't need self.
        */
        Rectangle { height: size, width: size }
    }

    fn area(&self) -> u32 {
        /* Method who return area of rectangle.
        */
        self.width * self.height
    }

    fn can_hold(&self, to_comp: &Rectangle) -> bool {
        /* Method who compare 2 rectangles.
        */
        if self.area() >= to_comp.area() {
            true
        }
        else {
            false
        }
    }

    fn is_square(&self) -> bool {
        /* Checking if square.
        */
        self.height == self.width
    }
}

fn main() {
    /* Main method of shapes program
    */
    let rect1 = Rectangle { height: 5, width: 5};
    let rect2 = Rectangle { height: 5, width: 5};
    let rect3 = Rectangle { height: 1, width: 1};
    let rect4 = Rectangle { height: 6, width: 6};
    let square1 = Rectangle::square(5);

    println!(
        "Your square: {:?}",
        square1
    );
    println!(
        "rect1 can hold rect2? {}",
        rect1.can_hold(&rect2)
    );
    println!(
        "rect1 can hold rect3? {}",
        rect1.can_hold(&rect3)
    );
    println!(
        "rect1 can hold rect4? {}",
        rect1.can_hold(&rect4)
    );
    println!(
        "rect1 is a square? {}",
        rect1.is_square()
    );
}
