/* Collections are stocked on the Heap so we dont need
to know they size in compile time
*/

fn print_vactor_value(vector: &Vec<i32>) {
    for val in vector {
        println!("We have the {} value", val);
    }
}

fn main() {
    /* Vector is a collection allow you to store values
    (with same type) next to each other in memory
    */
    let vec_empty: Vec<i32> = Vec::new();

    /* The vec! macro allow to infer the type of vector
    Especially useful when we declare vector with initial
    values.
    */
    let mut vec_fill = vec![1, 2, 3];

    /* To add new data in vector, obviously we can push
    only i32 values.
    */
    vec_fill.push(5);
    vec_fill.push(6);
    vec_fill.push(7);

    // We can access to vector value by index...
    let two: &i32 = &vec_fill[1];
    println!("The second value is {}", two);
    // ... or with `get()` method
    match vec_fill.get(2) {
        Some(third) => println!("The third element is {}", third),
        None => println!("Fail to get the third value."),
    }
    /* NOTE: With `get()` method the program will not crash if we
    try to access of an index out of range. Due to `Some`
    special structure that it can handle either a regular value
    or None.
    With index access method the program will panic.

    NOTE BIS: When we push value on a vector, all values will be
    copied on other memory place plus the new value. This behavior
    is inherent to nature of Vector. Value store in a contiguous
    memory space.
    */

    for val in &mut vec_fill {
        println!("The {} value is {}", val, val);
        /* To change value we can use the dereference operator on
        mutable reference.
        > *val += 10;
        */
    }

    // To remove the last element of a vector
    vec_fill.pop();
    print_vactor_value(&vec_fill);

    // We can bypass type limitation of a Vector with enum structure.
    enum CustomVect {
        Int(i32),
        Float(f64),
        Text(String),
    }
    let my_vector = vec![
        CustomVect::Int(3),
        CustomVect::Float(3.14),
        CustomVect::Text(String::from("My string.")),
    ];
}
