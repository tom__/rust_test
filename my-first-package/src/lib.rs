/*We split the module into few file*/
mod front_of_house;

/* Importing hosting into the scope - Work with Absolute and Relative path*/
use front_of_house::hosting;
/* With use we can also use `as` keyword*/
use front_of_house::back_of_house::Breakfast as Br;
/*Other Notes about use:

You can re-exporting name, for example:
>pub use front_of_house::hosting;
Make hosting::add_to_waitlist() usable by external code.

You can use nested path, see some examples below:
>use std::io;
>use std::cmp::Ordering;
OR
>use std::{cmp::Ordering, io};

>use std::io;
>use std::io::Write;
OR
>use std::io::{self, Write};

The glob operator is also valid, here we import
into the scope all pub items from std::collections:
>use std::collections::*;

Although using the glob pattern you can use prelude. Prelude
is a set of most using items from crate, when we decide to
import prelude the entire set are imported into the scope.
>use std::prelude::v1::*;
*/

pub fn eat_at_restaurant() {
    /*1: Absolute path - note that crate are implicit name
    for the root of the module
      2: Relative path
    crate::front_of_house::hosting::add_to_waitlist();
    front_of_house::hosting::add_to_waitlist();
    */

    /* Using `use` keyword allows to not specify full path at each time*/
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();

    /*Inner module struct manipulation.

    We can alter toast field from Breakfast struct because it's pub
    field. With seasonal_fruit this module won't compile.

    We can construct an instance of Breakfast only because we a pub
    associated function in the same scope of Breakfast struct so
    private field in this struct don't block instantiation.
    */
    let mut meal: Br = Br::summer("Rye");
    meal.toast = String::from("wheat");
    println!("Your choice are {}, good choice !", meal.toast);

    let order1 = crate::front_of_house::back_of_house::Appetizer::Soup;
    let order2 = crate::front_of_house::back_of_house::Appetizer::Salad;

}
