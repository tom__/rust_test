/*Definition of hosting module
*/

/* In this case all variant of enum are pub*/
pub enum Appetizer {
    Soup,
    Salad,
}

/*We can mix pub and private fields into struct*/
pub struct Breakfast {
    pub toast: String,
    seasonal_fruit: String,
}

impl Breakfast {
    pub fn summer(toast: &str) -> Breakfast {
        Breakfast {
            toast: String::from(toast),
            seasonal_fruit: String::from("apple"),
        }
    }
}

fn fix_incorrect_order() {
    cook_order();
    /*Using super to refer to serve_order function*/
    super::serving::serve_order();
}
fn cook_order() {
    println!("cook_order function");
}
