use std::io;
use std::io::Write;

#[derive(Debug)]
struct Human {
    name: String,
    age: u8,
}

fn create_human() -> Human {
    /* Creating of a human.
    */
    let mut raw_age: String = String::new();
    let mut new_human = Human {
        name: String::new(),
        age: 0,
    };

    println!("Enter your name:");
    io::stdin().read_line(&mut new_human.name)
        .expect("Bad choice Harry.");

    println!("Enter your age");
    io::stdin().read_line(&mut raw_age)
        .expect("Your age is not valid, liar!");
     let parsed_age: u8 = raw_age.parse::<u8>().unwrap();
    new_human.age = parsed_age;
    new_human
}

fn main() {
    /* Main function of show-struct program.
    */
    let mut choice: String = String::new();

    let mut _exit: bool = false;

    println!("Welcome to show-struct project:");
    println!("Enter a command:");

    while !_exit {
        println!("> ");
        io::stdout().flush()
            .expect("Stdout can't be flushed");

        io::stdin().read_line(&mut choice)
            .expect("Bad choice Harry.");

        match choice.as_str().trim() {
            "c" => {
                println!("show command, WOW!");
                println!("Your human: {:?}", create_human());
                continue;
            },
            "q" => {
                println!("Exiting...");
            },
            _ => {
                println!("Junck");
                continue;
            },
        };
        _exit = true;
    }
}
