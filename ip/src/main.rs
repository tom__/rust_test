#[derive(Debug)]
enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

#[derive(Debug)]
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Read(String),
    ChangeColor(i32, i32, i32),
}

impl Message {
    fn call(&self) -> &str {
        "Calling"
    }
}

fn main() {
    let ipv4_1 = IpAddr::V4(127, 0, 0, 1);
    let ipv6_1 = IpAddr::V6(String::from("::1"));
    let msg1 = Message::Read(String::from("Hello"));

    println!(
        "Show your IPv4: {:?}",
        ipv4_1,
    );
    println!(
        "Show your IPv6: {:?}",
        ipv6_1,
    );
    println!(
        "your message: {:?}",
        msg1.call(),
    );
    println!(
        "your message: {:?}",
        msg1.call(),
    );
}
