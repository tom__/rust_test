use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn is_equal(sec: u32, dummy_nbr: u32) -> bool {
    /* Checking if the numbers are equals and return bool
    */
    match dummy_nbr.cmp(&sec) {
        Ordering::Less => {
            println!("Too small");
            false
        },
        Ordering::Greater => {
            println!("Too big");
            false
        },
        Ordering::Equal => {
            println!("You win");
            true
        },
    }
}

fn what_your_guess() -> u32 {
    /* Take user input and check if it's a number
    */
    loop {
        let mut _nbr = String::new();

        // Read user input
        println!("Please enter your number :");

        io::stdin().read_line(&mut _nbr)
            .expect("Failed to read your number");
        // String to u32
        let _nbr: u32 = match _nbr.trim().parse() {
            Ok(num) => break num,
            Err(e) => {
                println!("Not today Harry: {}", e);
                continue;
            },
        };
    }
}

fn main() {
    /* Main function of guess number game
    */
    println!("Guess number!");

    let secret = rand::thread_rng().gen_range(1, 101);
    println!("secret is {}", secret);
    loop {
        // Asking user input
        let _nbr: u32 = what_your_guess();
        println!("You choose the number: {}", _nbr);

        // Checking victory
        match is_equal(secret, _nbr) {
            false => continue,
            true => break,
        };
    }
}

