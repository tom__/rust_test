use std::io;

const DAYS: [&str; 7] = ["monday", "tuesday", "wednesday",
                         "thursday", "friday", "saturday",
                         "sunday"];

fn ask_number_day(nbr: u8) -> (usize, String) {
    /* Taking a number and return the day
    */
    let mut nbr: usize = (nbr - 1).unwrap();
    (nbr, DAYS[nbr.clone()].to_string())
}

fn main() {
    /* Some code with array and vector
    */
    let mut day_nbr: String;

    let mut i: u8 = 0;

    for day in DAYS.iter() {
        i = i + 1;
        println!("The {} day is {}", i, day);
    }

    println!("Give a number and i'll give you the corresponding day:");
    let day_nbr = io::stdin().read_line(&mut day_nbr)
        .expect("Not expecting that!");
    let day_nbr = day_nbr.parse();

    let (day_nbr, day_resp) = ask_number_day(day_nbr.clone());
    println!("The day {} is {}.", day_nbr, day_resp);
}
