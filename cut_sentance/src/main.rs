use std::io;
use std::io::stdout;
use std::io::Write;
fn enter_blabla() -> std::string::String {
    /* Getting blabla from user and return this shit
    */
    let mut _bla = String::new();

    println!("Enter a small sentance");
    io::stdin().read_line(&mut _bla)
        .expect("Nop, not a sentance dummy!");
    _bla

}

fn do_retry() -> bool {
    /* Asking for retry or not, return bool
    */
    let mut _raw_choice: String = "yes".to_string();

    println!("If you want to retry, enter yes:");
    io::stdin().read_line(&mut _raw_choice)
        .expect("Bad choice");

    match _raw_choice.as_str().trim() {
        "yes" => true,
        _ => false,
    }
}

fn words_by_words(to_align: String) {
    /* Printing to the screen 1 words from sentance by line
    */
    let mut words_count: i32 = 0;

    for word in to_align.split(" ") {
        print!("{} | ", word);
        words_count += 1;
    }
    stdout().flush()
        .expect("Flush failed");
    println!("Total words: {}", words_count);
}

fn main() {
    /* Main function of cut_sentance
    */
    println!("Welcome to cut_sentance analyzer!");

    let mut to_cut: String;
    loop {
        to_cut = enter_blabla();

        words_by_words(to_cut);

        match do_retry() {
            false => break,
            true => continue,
        }
    }
}
